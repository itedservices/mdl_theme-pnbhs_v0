<?php

/*
 * @author    Shaun Daubney
 * @package   theme_pnbhs
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

    // Basic Heading
    $name = 'theme_pnbhs/basicheading';
    $heading = get_string('basicheading', 'theme_pnbhs');
    $information = get_string('basicheadingdesc', 'theme_pnbhs');
    $setting = new admin_setting_heading($name, $heading, $information);
    $settings->add($setting);
	
// Logo file setting
$name = 'theme_pnbhs/logo';
$title = get_string('logo','theme_pnbhs');
$description = get_string('logodesc', 'theme_pnbhs');
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$settings->add($setting);	

// Hide Menu
$name = 'theme_pnbhs/hidemenu';
$title = get_string('hidemenu','theme_pnbhs');
$description = get_string('hidemenudesc', 'theme_pnbhs');
$default = 1;
$choices = array(1=>get_string('yes',''), 0=>get_string('no',''));
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$settings->add($setting);

// Email url setting

$name = 'theme_pnbhs/emailurl';
$title = get_string('emailurl','theme_pnbhs');
$description = get_string('emailurldesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$settings->add($setting);

// Custom CSS file
$name = 'theme_pnbhs/customcss';
$title = get_string('customcss','theme_pnbhs');
$description = get_string('customcssdesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtextarea($name, $title, $description, $default);
$setting->set_updatedcallback('theme_reset_all_caches');
$settings->add($setting);

	// Frontpage Heading
    $name = 'theme_pnbhs/frontpageheading';
    $heading = get_string('frontpageheading', 'theme_pnbhs');
    $information = get_string('frontpageheadingdesc', 'theme_pnbhs');
    $setting = new admin_setting_heading($name, $heading, $information);
    $settings->add($setting);

// Title Date setting

$name = 'theme_pnbhs/titledate';
$title = get_string('titledate','theme_pnbhs');
$description = get_string('titledatedesc', 'theme_pnbhs');
$default = 1;
$choices = array(1=>get_string('yes',''), 0=>get_string('no',''));
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$settings->add($setting);

// General Alert setting
$name = 'theme_pnbhs/generalalert';
$title = get_string('generalalert','theme_pnbhs');
$description = get_string('generalalertdesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$settings->add($setting);

// Snow Alert setting
$name = 'theme_pnbhs/snowalert';
$title = get_string('snowalert','theme_pnbhs');
$description = get_string('snowalertdesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$settings->add($setting);

    // Colour Heading
    $name = 'theme_pnbhs/colourheading';
    $heading = get_string('colourheading', 'theme_pnbhs');
    $information = get_string('colourheadingdesc', 'theme_pnbhs');
    $setting = new admin_setting_heading($name, $heading, $information);
    $settings->add($setting);
	
// Background colour setting
$name = 'theme_pnbhs/backcolor';
$title = get_string('backcolor','theme_pnbhs');
$description = get_string('backcolordesc', 'theme_pnbhs');
$default = '#fafafa';
$previewconfig = NULL;
$setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
$settings->add($setting);

// Graphic Wrap (Background Image)
$name = 'theme_pnbhs/backimage';
$title=get_string('backimage','theme_pnbhs');
$description = get_string('backimagedesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
$settings->add($setting);

// Graphic Wrap (Background Position)
$name = 'theme_pnbhs/backposition';
$title = get_string('backposition','theme_pnbhs');
$description = get_string('backpositiondesc', 'theme_pnbhs');
$default = 'no-repeat';
$choices = array('no-repeat'=>get_string('backpositioncentred','theme_pnbhs'), 'no-repeat fixed'=>get_string('backpositionfixed','theme_pnbhs'), 'repeat'=>get_string('backpositiontiled','theme_pnbhs'), 'repeat-x'=>get_string('backpositionrepeat','theme_pnbhs'));
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$settings->add($setting);

// Menu hover background colour setting
$name = 'theme_pnbhs/menuhovercolor';
$title = get_string('menuhovercolor','theme_pnbhs');
$description = get_string('menuhovercolordesc', 'theme_pnbhs');
$default = '#f42941';
$previewconfig = NULL;
$setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
$settings->add($setting);	
	
	// Footer Options Heading
    $name = 'theme_pnbhs/footeroptheading';
    $heading = get_string('footeroptheading', 'theme_pnbhs');
    $information = get_string('footeroptdesc', 'theme_pnbhs');
    $setting = new admin_setting_heading($name, $heading, $information);
    $settings->add($setting);
	
// Copyright setting

$name = 'theme_pnbhs/copyright';
$title = get_string('copyright','theme_pnbhs');
$description = get_string('copyrightdesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$settings->add($setting);

// CEOP
$name = 'theme_pnbhs/ceop';
$title = get_string('ceop','theme_pnbhs');
$description = get_string('ceopdesc', 'theme_pnbhs');
$default = '';
$choices = array(''=>get_string('ceopnone','theme_pnbhs'), 'http://www.thinkuknow.org.au/site/report.asp'=>get_string('ceopaus','theme_pnbhs'), 'http://www.ceop.police.uk/report-abuse/'=>get_string('ceopuk','theme_pnbhs'));
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
$settings->add($setting);

// Disclaimer setting
$name = 'theme_pnbhs/disclaimer';
$title = get_string('disclaimer','theme_pnbhs');
$description = get_string('disclaimerdesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$settings->add($setting);	

	// Social Icons Heading
    $name = 'theme_pnbhs/socialiconsheading';
    $heading = get_string('socialiconsheading', 'theme_pnbhs');
    $information = get_string('socialiconsheadingdesc', 'theme_pnbhs');
    $setting = new admin_setting_heading($name, $heading, $information);
    $settings->add($setting);
	
// Website url setting

$name = 'theme_pnbhs/website';
$title = get_string('website','theme_pnbhs');
$description = get_string('websitedesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$settings->add($setting);

// Facebook url setting

$name = 'theme_pnbhs/facebook';
$title = get_string('facebook','theme_pnbhs');
$description = get_string('facebookdesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$settings->add($setting);

// Twitter url setting

$name = 'theme_pnbhs/twitter';
$title = get_string('twitter','theme_pnbhs');
$description = get_string('twitterdesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$settings->add($setting);

// Google+ url setting

$name = 'theme_pnbhs/googleplus';
$title = get_string('googleplus','theme_pnbhs');
$description = get_string('googleplusdesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$settings->add($setting);

// Flickr url setting

$name = 'theme_pnbhs/flickr';
$title = get_string('flickr','theme_pnbhs');
$description = get_string('flickrdesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$settings->add($setting);

// Pinterest url setting

$name = 'theme_pnbhs/pinterest';
$title = get_string('pinterest','theme_pnbhs');
$description = get_string('pinterestdesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$settings->add($setting);

// Instagram url setting

$name = 'theme_pnbhs/instagram';
$title = get_string('instagram','theme_pnbhs');
$description = get_string('instagramdesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$settings->add($setting);

// LinkedIn url setting

$name = 'theme_pnbhs/linkedin';
$title = get_string('linkedin','theme_pnbhs');
$description = get_string('linkedindesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$settings->add($setting);

// YouTube url setting

$name = 'theme_pnbhs/youtube';
$title = get_string('youtube','theme_pnbhs');
$description = get_string('youtubedesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$settings->add($setting);

// Apple url setting

$name = 'theme_pnbhs/apple';
$title = get_string('apple','theme_pnbhs');
$description = get_string('appledesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$settings->add($setting);

// Android url setting

$name = 'theme_pnbhs/android';
$title = get_string('android','theme_pnbhs');
$description = get_string('androiddesc', 'theme_pnbhs');
$default = '';
$setting = new admin_setting_configtext($name, $title, $description, $default);
$settings->add($setting);

}

